# README #

Solucion prueba proceso GlobalLogic

En el correo van los diagramas de solucion junto con el de componentes , ademas de eso va adjunto un proyecto postman con los endpoints utilizados.

A modo de resumen se realizo una aplicacion SpringBoot con 3 servicios para hacer la prueba , se creo una base de datos en memoria (H2), se generaron algunos
test unitarios en spock.


Antes de comenzar clonar el proyecto con el siguiente enlace:

git clone https://bcarrascoveas@bitbucket.org/bcarrascoveas/test-globallogic.git

1)Levantar el servicio con el siguiente comando despues de descargarlo

gradlew clean

gradlew build

gradlew test

gradlew bootrun

2)Verificar que el puerto en el que se haya levantado sea el 8080.

3) Antes que nada hay que generar un token para usar los servicios , con el siguiente endpoint que es un GET

http://localhost:8080/createToken/test@test.cl

El correo se puede cambiar pero para efecto de la generacion no afecta.

la respuesta de este servicio es la siguiente

eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJ0ZXN0SldUIiwic3ViIjoidGVzdEB0ZXN0LmNsIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYxMDM1MjkzOSwiZXhwIjoxNjEwMzUzNTM5fQ.44cczEKIz-P8OGbd7Itw0h9oKO9k92wOG54dRZSu1L_JOWItraldf934duR-wSbRRzmiOIA7KXTg1Teva4E9Lg


4)Utilizar el siguiente endpoint para agregar usuarios.

http://localhost:8080/searchUsers

Para mayor facilidad utilizar postman para poder crear la llamada al servicio.
El servicio esta construido como POST. En Authorization hay que escojer tipo bearer token y copiar el token generado anteriormente

El siguiente RQ se utilizara en el Body del Request para invocar al servicio.

{
   "name":"Juan Rodriguez",
   "email":"juanrodriguez.wd@aaa.cq",
   "password":"Aaaaaa11",
   "phones":[
      {
         "number":"1234567",
         "citycode":"1",
         "contrycode":"57"
      }
   ]
}

Algunas consideraciones es que el servicio valida que el email corresponda a un formato correo electronico y asi mismo que la password tenga
la estructura de una letra Mayuscula,letras minusculas y dos digitos Ej : "Aabcdefg12"

La respuesta del servicio se deberia asemejar a la siguiente:

{
    "email": "juanrodriguez.wd@aaa.cv",
    "id": "bf9f5720-33af-48bd-9a4c-0eadc348a0a7",
    "created": "Mon Jan 11 02:51:51 CLST 2021",
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJ0ZXN0SldUIiwic3ViIjoianVhbnJvZHJpZ3Vlei53ZEBhYWEuY3YiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjEwMzQ0MzExLCJleHAiOjE2MTAzNDQ5MTF9.Z_fnSEkpIGAvGk-mYi0BAAG6ItoRaKIuvHzoSMuEtaz7qiT96-NTtQXlY-oo5YUd7eS_gOkFXkej9Wcvx5UUtg",
    "last_login": "Mon Jan 11 02:51:51 CLST 2021",
    "isactive": true
}

En el caso de que el servicio arroje algun error, estos son los siguientes.

Correo Invalido:

{
    "mensaje": "Formato Incorrecto : Email"
}

Password Invalido:

{
    "mensaje": "Formato Incorrecto : Password"
}

Usuario Correo ya existe:

{
    "mensaje": "El correo ya registrado"
}



5)Para validar que se hayan creado los usuarios se disponibilizo el siguiente enpoints para validar. Tambien hay que agregale la Authorization Bearer Token.
Se puede usar el mismo que ya se creo.

http://localhost:8080/searchUsers


A diferencia del anterior este es un servicio GET el cual devuelve la informacion almacenada en la BD, devuelve todos los usuarios y esta es la respuesta.

[
    {
        "id": "d73129b2-a909-41e4-9434-9df94035ce3d",
        "name": "Juan Rodriguez",
        "email": "juanrodriguez.wd@aaa.cv",
        "password": "Aabcdefg11",
        "created": "2021-01-11T05:56:27.621+00:00",
        "modified": "2021-01-11T05:56:27.621+00:00",
        "last_login": "2021-01-11T05:56:27.621+00:00",
        "token": "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJ0ZXN0SldUIiwic3ViIjoianVhbnJvZHJpZ3Vlei53ZEBhYWEuY3YiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjEwMzQ0NTg3LCJleHAiOjE2MTAzNDUxODd9.nxOdUW0bvUlJg9c3DPJYu8beZg8I1kb9lIZdJohhkxJ7Vo7_vSPflaUmDkURVcQI5C2J33G3xUGSPYkYGcN1Qw",
        "isactive": true
    }
]

