package com.example.test

import org.mockito.Mockito
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

import com.example.test.entity.UserEntity
import com.example.test.repository.UserDao
import com.example.test.repository.UserRepository

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
@DataJpaTest
class BbTest extends Specification{	
	
	
	UserDao userDao = Mockito.mock(UserDao.class);
	
	UserRepository userRepo = Mockito.mock(UserRepository.class);
	
	UserEntity user
	
	void setup(){
		user = new UserEntity()
		user.setId("123");
		user.setCreated(new Date())
		user.setEmail("test@test.cl")
		user.setIsactive(false)
		user.setLast_login(new Date())
		user.setModified(new Date())
		user.setName("Test")
		user.setPassword("123Test")
		user.setToken("123abc")
	}
	
	def "Inserta en bd"(){
		when:"Ejecuta insert BD"
			userRepo.save(user)
		then: "Inserta Correcto"
	}
	
	public void "Busqueda Correo"() {
		given: "a user mail "
		String email = "www@aaa.cl"		
		
		when: "we search for the user"
		Integer value = userDao.findByEmailNamedQuery(email)
	  
		then: "Si no existe"
		value == 0
	 }	
}
