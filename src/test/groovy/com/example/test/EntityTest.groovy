package com.example.test

import javax.persistence.Column
import javax.persistence.Id
import javax.persistence.Lob

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

import com.example.test.domain.ErrorMsg
import com.example.test.domain.Phone
import com.example.test.domain.Response
import com.example.test.domain.User
import com.example.test.entity.UserEntity
import com.examplo.test.util.ValidationUtil
import com.fasterxml.jackson.annotation.JsonProperty

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
class EntityTest extends Specification{	

	UserEntity userEntity;	
	
	void setup(){
		userEntity = new UserEntity();		
	}
	
	def "Valida Objeto UserEntity"(){
		when:"Setea objeto UserEntity"
		userEntity.setId("a");
		userEntity.setName("b");
		userEntity.setEmail("c");
		userEntity.setPassword("d");
		userEntity.setCreated(new Date());
		userEntity.setModified(new Date());
		userEntity.setLast_login(new Date());
		userEntity.setToken("f");
		userEntity.setIsactive(true);		
		then: "Se valida objeto"
		userEntity.getId().equals("a");
		userEntity.getName().equals("b");
		userEntity.getEmail().equals("c");
		userEntity.getPassword().equals("d");
		userEntity.getCreated() != null;
		userEntity.getModified() != null;
		userEntity.getLast_login() != null;
		userEntity.getToken().equals("f");
		userEntity.isIsactive() == true;		
	}	
}
