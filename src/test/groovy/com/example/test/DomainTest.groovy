package com.example.test

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

import com.example.test.domain.ErrorMsg
import com.example.test.domain.Phone
import com.example.test.domain.Response
import com.example.test.domain.User
import com.examplo.test.util.ValidationUtil
import com.fasterxml.jackson.annotation.JsonProperty

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
class DomainTest extends Specification{	
	
	ErrorMsg error
	Phone phone;
	Response response
	User user
	
	
	void setup(){
		error = new ErrorMsg();
		phone = new Phone();
		response = new Response();
		user = new User();		
	}
	
	def "Valida Objeto ErrorMsg"(){
		when:"Setea objeto ErrorMsg"
		error.setMensaje("");
		then: "Se valida objeto"
		error != null;
	}
	
	def "Valida Objeto ErrorMsg mensaje"(){
		when:"Setea objeto ErrorMsg"
		error.setMensaje("asd");
		String msg = "asd";
		then: "Se valida objeto"
		error.getMensaje().equals(msg);
	}
	
	def "Valida Objeto Phone"(){
		when:"Setea objeto Phone"
		phone.setNumber("");
		phone.setCityCode("");
		phone.setCountryCode("");
		then: "Se valida objeto"
		phone != null;	
	}
	
	def "Valida Objeto Phone atributos"(){
		when:"Setea objeto Phone"
		phone.setNumber("a");
		phone.setCityCode("b");
		phone.setCountryCode("c");
		then: "Se valida objeto"
		phone.getCityCode().equals("b");
		phone.getCountryCode().equals("c");
		phone.getNumber().equals("a");
	}
	
	def "Valida Objeto Response"(){
		when:"Setea objeto Response"
		response.setEmail("");
		response.setLastLogin("");
		response.setToken("");
		then: "Se valida objeto"
		response != null;

	}
	

	def "Valida Objeto Response atributos"(){
		when:"Setea objeto Response"
		response.setId("a")
		response.setEmail("b");
		response.setLastLogin("c");
		response.setToken("d");
		response.setCreated("e");
		response.setActive(true);		
		then: "Se valida objeto"
		response.getCreated().equals("e");
		response.getEmail().equals("b");
		response.getLastLogin().equals("c");
		response.getId().equals("a");
		response.getToken().equals("d");
		response.isActive() == true;

	}
	
	def "Valida Objeto User"(){
		when:"Setea objeto User"
		user.setName("");
		user.setEmail("");
		user.setPassword("");
		user.setToken("");
		then: "Se valida objeto"
		user != null;	
	}	
	
	def "Valida Objeto User atributos"(){
		when:"Setea objeto User"
		user.setName("a");
		user.setEmail("b");
		user.setPassword("c");
		user.setToken("d");
		user.setPhones(new ArrayList());
		then: "Se valida objeto"
		user.getEmail().equals("b");
		user.getName().equals("a");
		user.getPassword().equals("c")
		user.getToken().equals("d");
		user.getPhones() != null;
	}
}
