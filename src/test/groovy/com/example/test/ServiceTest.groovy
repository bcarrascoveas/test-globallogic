package com.example.test

import org.springframework.boot.test.context.SpringBootTest

import com.example.test.service.UserImpl

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
class ServiceTest extends Specification{	
	
	def "Generar Token Correcto"(){
		when:		
		UserImpl service = new UserImpl();
		String token = service.createToken('asd@asd.cl');		
		then: "Se valida el token"		
		token != null;
	}
	
	
}
