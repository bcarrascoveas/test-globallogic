package com.example.test

import org.springframework.boot.test.context.SpringBootTest

import com.example.test.exception.ServiceException
import com.examplo.test.util.ValidationUtil

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
@SpringBootTest
class RegexTest extends Specification{
	
	private static final String testEmail = "test@test.cl"
	private static final String testEmailNotOk = "test@test"
	private static final String passwordNotOk = "aaas1";
	private static final String passwordOk = "Aabcdefg11";
	
	ValidationUtil validate
	
	void setup(){
		validate = new ValidationUtil()		
	}
	
	def "Valida Correo Error"(){		
		when:"ejecuto validacion de correo"
			validate.validateMail(testEmailNotOk)
		then: "valida exception"
			ServiceException ex = thrown()
			ex.getMensaje().equals("Formato Incorrecto : Email")			
	}
	
	def "Valida Correo OK"(){
		when:"ejecuto validacion de correo"
			validate.validateMail(testEmail)
		then: "valida correo ok"
	}
	
	def "Valida Password Error"(){
		when:"ejecuto validacion de password"
			validate.validatePass(passwordNotOk)
		then: "valida exception"
			ServiceException ex = thrown()
			ex.getMensaje().equals("Formato Incorrecto : Password")
	}
	
	def "Valida Password OK"(){
		when:"ejecuto validacion de password"
			validate.validatePass(passwordOk)
		then: "valida password ok"
	}	
}
