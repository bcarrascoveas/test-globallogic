package com.examplo.test.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.test.exception.ServiceException;

public class ValidationUtil {

	private static String mailRegex = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
	private static String passwordRegex = "^(?=.*[A-Z]{1})(?=.*[a-z])(?=.*[0-9]{2}).*$";

	public void validateMail(String field) throws ServiceException {
		validateField(field, mailRegex, "Email");
	}

	public void validatePass(String field) throws ServiceException {
		validateField(field, passwordRegex,"Password");
	}

	private void validateField(String field, String regex , String type) throws ServiceException {
		Pattern pat = Pattern.compile(regex);
		Matcher mat = pat.matcher(field);
		if (!mat.matches()) {
			throw new ServiceException("Formato Incorrecto : " + type);
		}
	}
}
