package com.example.test.exception;

public class ServiceException extends Exception{

	private static final long serialVersionUID = 1210859605028627524L;

	private String mensaje;
	
	public ServiceException (String msg) {
		this.setMensaje(msg);
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
