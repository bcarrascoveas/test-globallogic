package com.example.test.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Phone implements Serializable {

	private static final long serialVersionUID = 5834059074163055209L;

	private String number;

	@JsonProperty("citycode")
	private String cityCode;

	@JsonProperty("contrycode")
	private String countryCode;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
