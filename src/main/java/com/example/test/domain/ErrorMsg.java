package com.example.test.domain;

import java.io.Serializable;

public class ErrorMsg implements Serializable {

	private static final long serialVersionUID = -2959936461088979379L;
	private String mensaje;

	public ErrorMsg(String msg) {
		this.mensaje = msg;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
