package com.example.test.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response implements Serializable{

	private static final long serialVersionUID = 7208585590247552568L;

	private String email;
	
	private String id;
	private String created;
	
	@JsonProperty("last_login")
	private String lastLogin;
	
	private String token;
	
	@JsonProperty("isactive")
	private boolean isActive;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}	
}
