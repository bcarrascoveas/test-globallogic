package com.example.test.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.domain.ErrorMsg;
import com.example.test.domain.Response;
import com.example.test.domain.User;
import com.example.test.entity.UserEntity;
import com.example.test.exception.ServiceException;
import com.example.test.service.UserService;

@RestController
public class CreateController {

	final static Logger logger = Logger.getLogger(CreateController.class);

	@Autowired
	UserService userService;

	@PostMapping("/createUser")
	public ResponseEntity<?> createUser(@RequestHeader HttpHeaders headers, @RequestBody User user)
			throws ServiceException {
		try {
			String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
			user.setToken(token);
			return new ResponseEntity<Response>(userService.create(user), HttpStatus.OK);
		} catch (ServiceException e) {
			ErrorMsg error = new ErrorMsg(e.getMensaje());
			return new ResponseEntity<ErrorMsg>(error, HttpStatus.OK);
		}
	}

	@GetMapping("/searchUsers")
	public ResponseEntity<?> searchUsers() throws ServiceException {
		return new ResponseEntity<List<UserEntity>>(userService.searchAll(), HttpStatus.OK);
	}

	@GetMapping("/createToken/{email}")
	public ResponseEntity<?> createToken(@PathVariable String email) throws ServiceException {
		return new ResponseEntity<String>(userService.createToken(email), HttpStatus.OK);
	}
}
