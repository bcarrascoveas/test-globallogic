package com.example.test.service;

import java.util.List;

import com.example.test.domain.Response;
import com.example.test.domain.User;
import com.example.test.entity.UserEntity;
import com.example.test.exception.ServiceException;

public interface UserService {
	public Response create(User user) throws ServiceException;
	
	public List<UserEntity> searchAll()throws ServiceException;
	
	public String createToken(String email) throws ServiceException;
}
