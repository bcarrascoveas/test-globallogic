package com.example.test.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.example.test.domain.Response;
import com.example.test.domain.User;
import com.example.test.entity.UserEntity;
import com.example.test.exception.ServiceException;
import com.example.test.repository.UserDao;
import com.example.test.repository.UserRepository;
import com.examplo.test.util.ValidationUtil;
import com.google.gson.Gson;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserImpl implements UserService {

	final static Logger logger = Logger.getLogger(UserImpl.class);

	@Autowired
	UserRepository userRepo;

	@Autowired
	UserDao userDao;

	/**
	 * Creacion de usuarios a partir del objeto User
	 */
	@Override
	public Response create(User user) throws ServiceException {

		logger.info("Creando Usuario.....");
		Response response = new Response();
		Gson gson = new Gson();
		String request = gson.toJson(user);
		logger.info("Request  : " + request);
		ValidationUtil validation = new ValidationUtil();
		validation.validateMail(user.getEmail());
		validation.validatePass(user.getPassword());
		UserEntity userBD = new UserEntity();
		userBD.setId(UUID.randomUUID().toString());
		userBD.setName(user.getName());
		userBD.setEmail(user.getEmail());
		userBD.setPassword(user.getPassword());
		userBD.setCreated(new Date());
		userBD.setModified(new Date());
		userBD.setLast_login(new Date());
		userBD.setToken(user.getToken());
		userBD.setIsactive(true);
		if (userDao.findByEmailNamedQuery(user.getEmail()) == 0) {
			logger.info("Insertando en bd.....");
			UserEntity userBdResult = userRepo.save(userBD);
			response.setCreated(userBdResult.getCreated().toString());
			response.setId(userBdResult.getId());
			response.setLastLogin(userBdResult.getLast_login().toString());
			response.setToken(userBdResult.getToken());
			response.setEmail(userBdResult.getEmail());
			response.setActive(userBdResult.isIsactive());
		} else {
			throw new ServiceException("El correo ya registrado");
		}
		return response;
	}

	/**
	 * Retorna todos los usuarios creados
	 */
	@Override
	public List<UserEntity> searchAll() throws ServiceException {
		return (List<UserEntity>) userRepo.findAll();
	}

	/**
	 * Creacion de token a partir del mail del usuario
	 */
	@Override
	public String createToken(String email) throws ServiceException {
		logger.info("Generando token .....");
		try {
			String secretKey = "mySecretKey";
			List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

			String token = Jwts.builder().setId("testJWT").setSubject(email)
					.claim("authorities",
							grantedAuthorities.stream().map(GrantedAuthority::getAuthority)
									.collect(Collectors.toList()))
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + 600000))
					.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
			return token;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error al crear el token");
		}
	}
}
