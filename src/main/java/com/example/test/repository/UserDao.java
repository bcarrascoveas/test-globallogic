package com.example.test.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

@Service
public class UserDao {

	private final EntityManager em;

	public UserDao(EntityManager em) {
		this.em = em;
	}

	public Integer findByEmailNamedQuery(String email) {
		try {
			Query nativeQuery = em.createNamedQuery("selectUserByMail");
			nativeQuery.setParameter(1, email);
			int result = (int) nativeQuery.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return 0;
		}
	}
}
